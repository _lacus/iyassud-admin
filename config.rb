cur_dir = File.dirname(__FILE__)
relative_assets = true

# DIRECTORIES

http_path = "/"
css_dir = "/"
sass_dir = "/"
images_dir = "img"
javascripts_dir = "js"
#http_images_path = "../img"

# IMPORT PATHS

#add_import_path "sass/library"
#add_import_path "sass/library/bootstrap-sass-33.5/stylesheets"
#add_import_path "sass/helpers"
#add_import_path "sass/helpers/font-awsome-4.4.0"

# OUTPUT

sourcemap = true
output_style = :compressed
environment = :development
